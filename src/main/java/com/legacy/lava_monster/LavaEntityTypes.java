package com.legacy.lava_monster;

import com.legacy.lava_monster.entity.LavaMonsterEntity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraftforge.registries.ObjectHolder;

@ObjectHolder(LavaMonsterMod.MODID)
public class LavaEntityTypes
{
	public static final EntityType<LavaMonsterEntity> LAVA_MONSTER = buildEntity("lava_monster", EntityType.Builder.create(LavaMonsterEntity::new, EntityClassification.MONSTER).immuneToFire().size(0.8F, 2.2F));

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(LavaMonsterMod.find(key));
	}
}