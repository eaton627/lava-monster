package com.legacy.lava_monster;

import com.legacy.lava_monster.entity.LavaMonsterEntity;

import net.minecraft.entity.EntitySpawnPlacementRegistry;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.attributes.GlobalEntityTypeAttributes;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.world.gen.Heightmap;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

@EventBusSubscriber(modid = LavaMonsterMod.MODID, bus = Bus.MOD)
public class LavaRegistryHandler
{
	@SubscribeEvent
	public static void onRegisterItems(Register<Item> event)
	{
		register(event.getRegistry(), "lava_monster_spawn_egg", new SpawnEggItem(LavaEntityTypes.LAVA_MONSTER, 0xda7522, 0xdab022, new Item.Properties().group(ItemGroup.MISC)));
	}

	@SubscribeEvent
	public static void onRegisterEntityTypes(Register<EntityType<?>> event)
	{
		register(event.getRegistry(), "lava_monster", LavaEntityTypes.LAVA_MONSTER);
		EntitySpawnPlacementRegistry.register(LavaEntityTypes.LAVA_MONSTER, EntitySpawnPlacementRegistry.PlacementType.ON_GROUND, Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, MonsterEntity::canSpawnOn);
		GlobalEntityTypeAttributes.put(LavaEntityTypes.LAVA_MONSTER, LavaMonsterEntity.registerAttributeMap().func_233813_a_());
	}

	static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, String name, T object)
	{
		object.setRegistryName(LavaMonsterMod.locate(name));
		registry.register(object);
	}
}