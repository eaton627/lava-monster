package com.legacy.lava_monster.client;

import com.legacy.lava_monster.LavaEntityTypes;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class LavaEntityRendering
{
	public static void init()
	{
		register(LavaEntityTypes.LAVA_MONSTER, LavaMonsterRenderer::new);
	}

	private static <T extends Entity> void register(EntityType<T> entityClass, IRenderFactory<? super T> renderFactory)
	{
		RenderingRegistry.registerEntityRenderingHandler(entityClass, renderFactory);
	}
}