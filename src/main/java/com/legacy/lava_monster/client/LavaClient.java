package com.legacy.lava_monster.client;

import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

public class LavaClient
{
	public static void initialization(FMLClientSetupEvent event)
	{
		LavaEntityRendering.init();
	}
}