package com.legacy.lava_monster.client;

import com.legacy.lava_monster.LavaMonsterMod;
import com.legacy.lava_monster.entity.LavaMonsterEntity;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.settings.GraphicsFanciness;
import net.minecraft.util.ResourceLocation;

public class LavaMonsterRenderer extends MobRenderer<LavaMonsterEntity, LavaMonsterModel<LavaMonsterEntity>>
{
	public static final ResourceLocation[] LAVA_MONSTER_TEXTURES;
	static
	{
		if (Minecraft.getInstance().gameSettings.field_238330_f_ != GraphicsFanciness.FAST)
		{
			LAVA_MONSTER_TEXTURES = new ResourceLocation[20];
		}
		else
		{
			LAVA_MONSTER_TEXTURES = new ResourceLocation[1];
		}

		String path = LavaMonsterMod.find("textures/entity/lava_monster_");
		for (int i = 0; i < LavaMonsterRenderer.LAVA_MONSTER_TEXTURES.length; i++)
		{
			LavaMonsterRenderer.LAVA_MONSTER_TEXTURES[i] = new ResourceLocation(path + Integer.toString(i) + ".png");
		}
	}

	public LavaMonsterRenderer(EntityRendererManager rendermanagerIn)
	{
		super(rendermanagerIn, new LavaMonsterModel<>(), 0.5F);
	}

	@Override
	protected void preRenderCallback(LavaMonsterEntity entitylivingbaseIn, MatrixStack matrixStackIn, float partialTickTime)
	{
		matrixStackIn.scale(1.25F, 1.25F, 1.25F);
	}

	@Override
	public ResourceLocation getEntityTexture(LavaMonsterEntity entity)
	{
		return Minecraft.getInstance().gameSettings.field_238330_f_ != GraphicsFanciness.FAST ? LavaMonsterRenderer.LAVA_MONSTER_TEXTURES[((LavaMonsterEntity) entity).getTextureIndex()] : LavaMonsterRenderer.LAVA_MONSTER_TEXTURES[0];
	}
}