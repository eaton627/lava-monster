package com.legacy.lava_monster;

import com.legacy.lava_monster.client.LavaClient;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(LavaMonsterMod.MODID)
public class LavaMonsterMod
{
	public static final String NAME = "Lava Monster";
	public static final String MODID = "lava_monster";

	public LavaMonsterMod()
	{
		ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, LavaMonsterConfig.CLIENT_SPEC);
		ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, LavaMonsterConfig.SERVER_SPEC);

		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () ->
		{
			FMLJavaModLoadingContext.get().getModEventBus().addListener(LavaClient::initialization);
		});

		MinecraftForge.EVENT_BUS.register(new LavaMonsterSpawnEvent());
	}

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}
}
