package com.legacy.lava_monster;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import com.google.common.collect.Lists;
import com.legacy.lava_monster.entity.LavaMonsterEntity;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.Difficulty;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

public class LavaMonsterSpawnEvent
{
	/// Handy properties for this class.
	public static final int SPAWN_FREQUENCY = LavaMonsterConfig.spawnFrequency;
	public static final boolean DEPTH_HAZARD = LavaMonsterConfig.depthHazard;
	public static final boolean SHALLOW_LAVA = LavaMonsterConfig.shallowLava;
	public static final boolean FLOWING_LAVA = LavaMonsterConfig.flowingLava;
	/// The max number of lava monsters around.
	private static final int maxNumberOfCreature = 10;

	/// Counter to the next spawn attempt.
	private int spawnTime = 0;
	/// A map of eligible spawning chunks.
	private HashMap<ChunkPos, Boolean> eligibleChunksForSpawning = new HashMap<ChunkPos, Boolean>();

	public LavaMonsterSpawnEvent()
	{
	}

	/// Returns true if a lava monster can spawn at the given location.
	public static boolean canLavaMonsterSpawnAtLocation(World world, BlockPos pos)
	{
		return (!LavaMonsterSpawnEvent.DEPTH_HAZARD || pos.getY() <= 16 || world.func_230315_m_().func_236040_e_()) && world.getBlockState(pos).getBlock() == Blocks.LAVA && (world.getBlockState(pos.up()).getBlock() == Blocks.LAVA);
	}

	/// Spawns lava monsters in the world. Returns the number spawned for debugging
	/// purposes.
	private int performSpawning(ServerWorld world)
	{
		if (++this.spawnTime < LavaMonsterSpawnEvent.SPAWN_FREQUENCY)
			return 0;

		this.eligibleChunksForSpawning.clear();

		for (PlayerEntity player : world.getPlayers())
		{
			int chunkX = MathHelper.floor(player.getPosX() / 16.0);
			int chunkZ = MathHelper.floor(player.getPosZ() / 16.0);
			byte spawnRange = 8; /// In chunks.
			for (int x = -spawnRange; x <= spawnRange; x++)
			{
				for (int z = -spawnRange; z <= spawnRange; z++)
				{
					boolean isEdge = x == -spawnRange || x == spawnRange || z == -spawnRange || z == spawnRange;

					ChunkPos chunkCoord = new ChunkPos(x + chunkX, z + chunkZ);

					if (!isEdge)
					{
						this.eligibleChunksForSpawning.put(chunkCoord, Boolean.valueOf(false));
					}
					else if (!this.eligibleChunksForSpawning.containsKey(chunkCoord))
					{
						this.eligibleChunksForSpawning.put(chunkCoord, Boolean.valueOf(true));
					}
				}
			}
		}

		int numberSpawned = 0;
		BlockPos spawnCoords = world.func_241135_u_(); //get spawn point

		if (this.getLavaMonsters(world).size() <= LavaMonsterSpawnEvent.maxNumberOfCreature * this.eligibleChunksForSpawning.size() / 256)
		{
			ArrayList<ChunkPos> chunks = new ArrayList<ChunkPos>(this.eligibleChunksForSpawning.keySet());
			Collections.shuffle(chunks);
			chunkIterator: for (ChunkPos chunkCoord : chunks)
			{
				if (!this.eligibleChunksForSpawning.get(chunkCoord).booleanValue())
				{
					BlockPos chunkPos = this.getRandomSpawningPointInChunk(world, chunkCoord.x, chunkCoord.z);
					int x = chunkPos.getX();
					int y = chunkPos.getY();
					int z = chunkPos.getZ();
					/*if (world.isBlockNormalCubeDefault(x, y, z, true))
					{
						continue;
					}*/
					byte groupRadius = 6;
					for (int groupSpawnAttempt = 3; groupSpawnAttempt-- > 0;)
					{
						int X = x;
						int Y = y;
						int Z = z;
						for (int spawnAttempt = 4; spawnAttempt-- > 0;)
						{
							X += world.rand.nextInt(groupRadius) - world.rand.nextInt(groupRadius);
							Y += world.rand.nextInt(1) - world.rand.nextInt(1);
							Z += world.rand.nextInt(groupRadius) - world.rand.nextInt(groupRadius);

							if (LavaMonsterSpawnEvent.canLavaMonsterSpawnAtLocation(world, new BlockPos(X, Y, Z)))
							{
								float posX = X + 0.5F;
								float posY = Y;
								float posZ = Z + 0.5F;

								if (!world.isPlayerWithin(posX, posY, posZ, 24))
								{
									float spawnX = posX - spawnCoords.getX();
									float spawnY = posY - spawnCoords.getY();
									float spawnZ = posZ - spawnCoords.getZ();
									float spawnDist = spawnX * spawnX + spawnY * spawnY + spawnZ * spawnZ;

									if (spawnDist >= 576.0F)
									{
										LavaMonsterEntity lavaMonster = new LavaMonsterEntity(LavaEntityTypes.LAVA_MONSTER, world);
										lavaMonster.setLocationAndAngles(posX, posY, posZ, world.rand.nextFloat() * 360.0F, 0.0F);
										Result canSpawn = ForgeEventFactory.canEntitySpawn(lavaMonster, world, posX, posY, posZ, null, SpawnReason.NATURAL);

										if (canSpawn == Result.ALLOW || canSpawn == Result.DEFAULT && lavaMonster.getCanSpawnHere())
										{
											numberSpawned++;

											world.addEntity(lavaMonster);

											if (!ForgeEventFactory.doSpecialSpawn(lavaMonster, world, posX, posY, posZ, null, null))
											{
												lavaMonster.onInitialSpawn(world, world.getDifficultyForLocation(new BlockPos(posX, posY, posZ)), SpawnReason.NATURAL, (ILivingEntityData) null, (CompoundNBT) null);
											}
											continue chunkIterator;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return numberSpawned;
	}

	/// Returns a randomized chunk position within the given chunk.
	private BlockPos getRandomSpawningPointInChunk(World world, int chunkX, int chunkZ)
	{
		Chunk chunk = world.getChunk(chunkX, chunkZ);
		int x = (chunkX << 4) + world.rand.nextInt(16);
		int z = (chunkZ << 4) + world.rand.nextInt(16);
		int y = world.rand.nextInt(chunk == null ? world.getHeight() : chunk.getTopFilledSegment() + 16 - 1);
		return new BlockPos(x, y, z);
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void onServerTick(TickEvent.WorldTickEvent event)
	{
		if (event.phase == TickEvent.Phase.START)
		{
			if (event.world instanceof ServerWorld && event.world.getGameRules().getBoolean(GameRules.DO_MOB_SPAWNING) && event.world.getDifficulty() != Difficulty.PEACEFUL)
			{
				this.performSpawning((ServerWorld) event.world);
			}
		}
	}

	public List<LavaMonsterEntity> getLavaMonsters(World world)
	{
		List<LavaMonsterEntity> list = Lists.newArrayList();
		Int2ObjectMap<Entity> entById = ObfuscationReflectionHelper.getPrivateValue(ServerWorld.class, (ServerWorld) world, "field_217498_x");
		for (Entity entity : (entById.values()))
		{
			if (entity instanceof LavaMonsterEntity && entity.isAlive())
			{
				list.add((LavaMonsterEntity) entity);
			}
		}

		return list;
	}
}