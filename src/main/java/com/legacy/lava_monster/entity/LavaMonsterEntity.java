package com.legacy.lava_monster.entity;

import com.legacy.lava_monster.LavaMonsterConfig;

import net.minecraft.block.AbstractFireBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.passive.SnowGolemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.SmallFireballEntity;
import net.minecraft.entity.projectile.SnowballEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;

public class LavaMonsterEntity extends MonsterEntity implements IRangedAttackMob
{
	// TODO figure out how to register this before attribute registry
	public static double MAX_HEALTH = 16.0D; // LavaMonsterConfig.monsterHealth;
	public double BASE_ARMOR = LavaMonsterConfig.monsterArmor;
	public double SPAWN_CHANCE = LavaMonsterConfig.spawnChance;

	/// Ticks until the next attack phase change.
	public int attackDelay = 0;
	/// True if the texture index is increasing.
	private boolean textureInc = true;
	/// Counter to next texture update.
	private byte textureTicks = 0;
	/// The current texture index.
	private byte textureIndex = 0;

	public LavaMonsterEntity(EntityType<? extends LavaMonsterEntity> type, World world)
	{
		super(type, world);

		this.setPathPriority(PathNodeType.WATER, -1.0F);
		this.setPathPriority(PathNodeType.LAVA, 0.0F);
		this.setPathPriority(PathNodeType.DANGER_FIRE, 0.0F);
		this.setPathPriority(PathNodeType.DAMAGE_FIRE, 0.0F);
		this.setPathPriority(PathNodeType.OPEN, 0.0F);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new SwimGoal(this));
		this.goalSelector.addGoal(3, new AvoidEntityGoal<>(this, SnowGolemEntity.class, 6.0F, 1.0D, 1.2D));
		this.goalSelector.addGoal(3, new LavaMonsterAttackGoal(this, 1.0D));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 1.0D));
		this.goalSelector.addGoal(6, new LookAtGoal(this, PlayerEntity.class, 8.0F));
		this.goalSelector.addGoal(6, new LookRandomlyGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)).setCallsForHelp());
		this.targetSelector.addGoal(1, new HurtByTargetGoal(this));
		this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, true));
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntitySize sizeIn)
	{
		return 1.5F;
	}

	public static AttributeModifierMap.MutableAttribute registerAttributeMap()
	{
		return MonsterEntity.func_234295_eP_().func_233815_a_(Attributes.field_233818_a_, MAX_HEALTH).func_233815_a_(Attributes.field_233819_b_, 35.0D).func_233815_a_(Attributes.field_233821_d_, 0.24F).func_233815_a_(Attributes.field_233823_f_, 3.0D).func_233815_a_(Attributes.field_233826_i_, 2.0D);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return null;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return SoundEvents.ITEM_BUCKET_EMPTY_LAVA;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return SoundEvents.ENTITY_GHAST_DEATH;
	}

	@Override
	public void livingTick()
	{
		super.livingTick();

		if (this.world.isRemote)
		{
			this.updateTexture();
		}
		else
		{
			if (this.isInLava())
				this.setMotion(this.getMotion().mul(1.9D, 1.0D, 1.9D));

			if (this.getAttackTarget() != null)
				this.getLookController().setLookPositionWithEntity(this.getAttackTarget(), 20.0F, 20.0F);

			this.attackDelay = Math.max(0, this.attackDelay - 1);

			if (this.isWet())
			{
				this.attackEntityFrom(DamageSource.DROWN, 1);
			}

			BlockPos pos = new BlockPos(MathHelper.floor(this.getPosX()), MathHelper.floor(this.getPosY()), MathHelper.floor(this.getPosZ()));

			// thanks timmy
			BlockState fire = this.getName().getUnformattedComponentText().equalsIgnoreCase("timmy") ? Blocks.FIRE.getDefaultState() : AbstractFireBlock.func_235326_a_(world, pos);

			if (this.world.isAirBlock(pos) && this.world.getGameRules().getBoolean(GameRules.MOB_GRIEFING) && this.world.getGameRules().getBoolean(GameRules.DO_FIRE_TICK) && fire.isValidPosition(this.world, pos))
			{
				this.world.setBlockState(pos, fire, 2);
			}
		}

		if (this.rand.nextInt(100) == 0)
		{
			this.world.addParticle(ParticleTypes.LAVA, this.getPosX() + (this.rand.nextDouble() - 0.5) * this.getWidth(), this.getPosY() + this.rand.nextDouble() * this.getHeight() + this.getHeight() / 2.0, this.getPosZ() + (this.rand.nextDouble() - 0.5) * this.getWidth(), 0.0, 0.0, 0.0);
			this.playSound(SoundEvents.BLOCK_LAVA_POP, 0.2F + this.rand.nextFloat() * 0.2F, 0.9F + this.rand.nextFloat() * 0.15F);
		}

		if (this.rand.nextInt(200) == 0)
		{
			this.playSound(SoundEvents.BLOCK_LAVA_AMBIENT, 0.2F + this.rand.nextFloat() * 0.2F, 0.9F + this.rand.nextFloat() * 0.15F);
		}
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
		{
			return false;
		}
		else if (source.getImmediateSource() instanceof SnowballEntity)
		{
			return super.attackEntityFrom(source, Math.max(3.0F, amount));
		}
		else
		{
			return super.attackEntityFrom(source, amount);
		}
	}

	@Override
	public void attackEntityWithRangedAttack(LivingEntity target, float distanceFactor)
	{
		SmallFireballEntity fireball = new SmallFireballEntity(this.world, this, target.getPosX() - this.getPosX(), target.getBoundingBox().minY + target.getHeight() / 2.0F - this.getPosY() - this.getHeight() / 2.0F, target.getPosZ() - this.getPosZ());
		fireball.setPosition(fireball.getPosX(), this.getPosY() + this.getHeight() - 0.5, fireball.getPosZ());
		this.playSound(SoundEvents.ENTITY_GHAST_SHOOT, 1.0F, 1.0F / (this.rand.nextFloat() * 0.4F + 0.8F));
		this.world.addEntity(fireball);
	}

	@Override
	protected void registerData()
	{
		super.registerData();
	}

	@Override
	public void writeAdditional(CompoundNBT compound)
	{
		super.writeAdditional(compound);
	}

	@Override
	public void read(CompoundNBT compound)
	{
		super.read(compound);
	}

	public boolean getCanSpawnHere() // areCollisionShapesEmpty areCollisionBoxesEmpty
	{
		String[] biomeArray = LavaMonsterConfig.extraBiomes.split(",");

		Biome biome = world.getBiome(this.getPositionUnderneath());
		boolean taggedProperly = BiomeDictionary.hasType(biome, BiomeDictionary.Type.OVERWORLD) || BiomeDictionary.hasType(biome, BiomeDictionary.Type.NETHER);
		boolean isWhitelisted = false;

		if (!taggedProperly)
		{
			for (String name : biomeArray)
			{
				ResourceLocation regName = new ResourceLocation(name);

				if (biome.getRegistryName().equals(regName))
				{
					// System.out.println(name + " is valid!");
					isWhitelisted = true;
				}
				/*else
					System.out.println(biome + " isn't valid!");*/

			}
		}

		return this.rand.nextDouble() < SPAWN_CHANCE && this.world.hasNoCollisions(this.getBoundingBox()) && this.world.hasNoCollisions(this, this.getBoundingBox()) && (taggedProperly || isWhitelisted);
	}

	public void updateTexture()
	{
		if (++this.textureTicks < 2)
			return;

		this.textureTicks = 0;
		this.textureIndex += this.textureInc ? 1 : -1;

		if (this.textureIndex < 0)
		{
			this.textureIndex = 1;
			this.textureInc = true;
		}
		else if (this.textureIndex > 19)
		{
			this.textureIndex = 18;
			this.textureInc = false;
		}
	}

	public int getTextureIndex()
	{
		return this.textureIndex;
	}

	@Override
	public ItemEntity entityDropItem(ItemStack stack, float offsetY)
	{
		if (stack.isEmpty())
		{
			return null;
		}
		else if (this.world.isRemote)
		{
			return null;
		}
		else
		{
			ItemEntity itementity = new ItemEntity(this.world, this.getPosX(), this.getPosY() + (double) offsetY, this.getPosZ(), stack);
			itementity.setDefaultPickupDelay();
			itementity.setInvulnerable(true);
			if (captureDrops() != null)
				captureDrops().add(itementity);
			else
				this.world.addEntity(itementity);
			return itementity;
		}
	}
}