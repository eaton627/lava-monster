package com.legacy.lava_monster;

import org.apache.commons.lang3.tuple.Pair;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;

@Mod.EventBusSubscriber(modid = LavaMonsterMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class LavaMonsterConfig
{
	public static final ForgeConfigSpec CLIENT_SPEC;
	public static final ForgeConfigSpec SERVER_SPEC;
	public static final ClientConfig CLIENT;
	public static final ServerConfig SERVER;

	/*public static double monsterHealth;*/
	public static double monsterArmor;
	public static double monsterRange;
	public static int attackCooldown;
	public static int attackShots;
	public static int attackSpacing;
	public static int attackWindup;
	public static boolean depthHazard;
	public static boolean flowingLava;
	public static boolean shallowLava;
	public static double spawnChance;
	public static int spawnFrequency;
	/*public static String dimensionWhitelist;*/
	public static String extraBiomes;

	static
	{
		{
			final Pair<ClientConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = pair.getLeft();
			CLIENT_SPEC = pair.getRight();
		}
		{
			final Pair<ServerConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(ServerConfig::new);
			SERVER = pair.getLeft();
			SERVER_SPEC = pair.getRight();
		}
	}

	private static String translate(String key)
	{
		return new String(LavaMonsterMod.MODID + ".config." + key + ".name");
	}

	@SubscribeEvent
	public static void onLoadConfig(final ModConfig.ModConfigEvent event)
	{
		ModConfig config = event.getConfig();

		if (config.getSpec() == CLIENT_SPEC)
		{
			ConfigBakery.bakeClient(config);
		}
		else if (config.getSpec() == SERVER_SPEC)
		{
			ConfigBakery.bakeServer(config);
		}
	}

	private static class ClientConfig
	{
		public ClientConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Client side changes.").push("client");
			builder.pop();
		}
	}

	private static class ServerConfig
	{
		/*public final ForgeConfigSpec.ConfigValue<Double> monsterHealth;*/
		public final ForgeConfigSpec.ConfigValue<Double> monsterArmor;
		public final ForgeConfigSpec.ConfigValue<Double> monsterRange;

		public final ForgeConfigSpec.ConfigValue<Integer> attackCooldown;
		public final ForgeConfigSpec.ConfigValue<Integer> attackShots;
		public final ForgeConfigSpec.ConfigValue<Integer> attackSpacing;
		public final ForgeConfigSpec.ConfigValue<Integer> attackWindup;

		public final ForgeConfigSpec.ConfigValue<Boolean> depthHazard;
		public final ForgeConfigSpec.ConfigValue<Boolean> flowingLava;
		public final ForgeConfigSpec.ConfigValue<Boolean> shallowLava;
		/*public final ForgeConfigSpec.ConfigValue<String> dimensionWhitelist;*/
		public final ForgeConfigSpec.ConfigValue<String> extraBiomes;

		public final ForgeConfigSpec.ConfigValue<Double> spawnChance;
		public final ForgeConfigSpec.ConfigValue<Integer> spawnFrequency;

		public ServerConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Server and Client side changes.").push("common");
			/*monsterHealth = builder.translation(translate("monsterHealth")).comment("Lava monsters' maximum health.").define("monsterHealth", 16.0D);*/
			monsterArmor = builder.translation(translate("monsterArmor")).comment("The amount of armor lava monsters have.").define("monsterArmor", 0.0D);
			monsterRange = builder.translation(translate("monsterRange")).comment("The distance a lava monster will begin attacking. (maximum of 35)").define("monsterRange", 17.0D);
			attackCooldown = builder.translation(translate("attackCooldown")).comment("Ticks a monster must wait after attacking before it can start winding up again.").define("attackCooldown", 80);
			attackShots = builder.translation(translate("attackShots")).comment("Number of fireballs shot with each attack.").define("attackShots", 3);
			attackSpacing = builder.translation(translate("attackSpacing")).comment("Ticks between each fireball shot in an attack.").define("attackSpacing", 6);
			attackWindup = builder.translation(translate("attackWindup")).comment("Ticks it takes before a monster can start an attack.").define("attackWindup", 60);
			depthHazard = builder.translation(translate("depthHazard")).comment("If true, lava monsters will not spawn above layer 16.").define("depthHazard", false);
			flowingLava = builder.translation(translate("flowingLava")).comment("If true, lava monsters do not require a source block to spawn.").define("flowingLava", false);
			shallowLava = builder.translation(translate("shallowLava")).comment("If true, lava monsters will be able to spawn in lava one block deep.").define("shallowLava", false);
			spawnChance = builder.translation(translate("spawnChance")).comment("The chance for a lava monster spawn attempt to be successful.").define("spawnChance", 0.05D);
			spawnFrequency = builder.translation(translate("spawnFrequency")).comment("The number of ticks between each lava monster spawn attempt.").define("spawnFrequency", 10);
			/*dimensionWhitelist = builder.translation(translate("dimensionWhitelist")).comment("Allows you to choose which dimensions the Lava Monster spawns in. Add dimensions by their registry name/id, and separate them with commas, no spaces.").define("dimensionWhitelist", "minecraft:overworld,minecraft:the_nether,goodnightsleep:nightmare");*/
			extraBiomes = builder.translation(translate("extraBiomes")).comment("Allows you to add to the biomes the Lava Monster can spawn in, this does not change the existing biomes, only adds extra ones. Separate with commas. (By default, the Lava Monster will spawn in biomes tagged as nether or overworld.)").define("extraBiomes", "good_nights_sleep:nightmare");
			builder.pop();
		}
	}

	@SuppressWarnings("unused")
	private static class ConfigBakery
	{
		private static ModConfig clientConfig;
		private static ModConfig serverConfig;

		public static void bakeClient(ModConfig config)
		{
			clientConfig = config;
		}

		public static void bakeServer(ModConfig config)
		{
			serverConfig = config;
			/*monsterHealth = SERVER.monsterHealth.get();*/
			monsterArmor = SERVER.monsterArmor.get();
			monsterRange = SERVER.monsterRange.get();
			attackCooldown = SERVER.attackCooldown.get();
			attackShots = SERVER.attackShots.get();
			attackSpacing = SERVER.attackSpacing.get();
			attackWindup = SERVER.attackWindup.get();
			depthHazard = SERVER.depthHazard.get();
			flowingLava = SERVER.flowingLava.get();
			shallowLava = SERVER.shallowLava.get();
			spawnChance = SERVER.spawnChance.get();
			spawnFrequency = SERVER.spawnFrequency.get();
			/*dimensionWhitelist = SERVER.dimensionWhitelist.get();*/
			extraBiomes = SERVER.extraBiomes.get();

		}
	}
}
